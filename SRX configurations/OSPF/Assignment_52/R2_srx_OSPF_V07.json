## Last commit: 2020-05-16 00:19:43 CEST by root
version 12.1X47-D15.4;
system {
    host-name vSRX-R2;
    time-zone Europe/Berlin;
    root-authentication {
        encrypted-password "$1$xH9xJoL6$MFOUYnZr4.Qj2NM24XInz/"; ## Rootpass
    }
    login {
        user per {
            uid 2020;
            class super-user;
            authentication {
                encrypted-password "$1$MqEo7eB1$baEcWYIkqTKrfVKWgRUqZ0"; ## SECRET-DATA
            }
        }
    }
    services {
        ssh;
        dhcp-local-server {
            group DMZ {
                interface ge-0/0/3.0;
            }
            group USERLAN {
                interface ge-0/0/4.0;
            }
        }
    }
}
interfaces {
    ge-0/0/2 {
        unit 0 {
            family inet {
                /* VMnet5 Trust zone */
                address 10.10.10.1/28;
            }
        }
    }
    ge-0/0/3 {
        unit 0 {
            family inet {
                /* DMZ zone */
                address 192.168.12.1/24;
            }
        }
    }
    ge-0/0/4 {
        unit 0 {
            family inet {
                /* USERLAN Trust zone */
                address 192.168.13.1/24;
            }
        }
    }
    ge-0/0/5 {
        unit 0 {
            family inet {
                /* VMnet8 Untrust zone */
                address 10.56.16.80/22;
            }
        }
    }
    ge-0/0/6 {
        unit 0 {
            family inet {
                /* SERVERLAN Trust zone */
                address 192.168.11.1/24;
            }
        }
    }
}
routing-options {
    static {
        /* Route to school router gateway */
        route 0.0.0.0/0 next-hop 10.56.16.1;
    }
}
protocols {
    ospf {
        export my-default-static-route-to-internet;
		export my-route-to-SERVERLAN;
        area 0.0.0.0 {
            interface ge-0/0/2.0;
        }
    }
}
policy-options {
    policy-statement my-default-static-route-to-internet {
        term accpt-my-default-route {
            from {
                protocol static;
                route-filter 0.0.0.0/0 exact;
            }
            then accept;
        }
    }
	policy-statement my-route-to-SERVERLAN {
        term accpt-my-route-to-SERVERLAN {
            from {
                route-filter 192.168.11.0/24 exact;
            }
            then accept;
        }
    }
}
security {
    nat {
        /* Changes the source address of egress packets */
        source {
            /* Multiple sets of rules can be set */
            rule-set trust-to-untrust {
                from zone trust;
                to zone untrust;
                /* Multiple rules can be set in each rule-set */
                rule rule-any-to-any {
                    match {
                        source-address 0.0.0.0/0;
                        destination-address 0.0.0.0/0;
                    }
                    then {
                        source-nat {
                            /* Use egress interface source address */
                            interface;
                        }
                    }
                }
            }
            rule-set DMZ-to-untrust {
                from zone DMZ;
                to zone untrust;
                /* Multiple rules can be set in each rule-set */
                rule rule-is-any-to-any {
                    match {
                        source-address 0.0.0.0/0;
                        destination-address 0.0.0.0/0;
                    }
                    then {
                        source-nat {
                            /* Use egress interface source address */
                            interface;
                        }
                    }
                }
            }
        }
        destination {
            pool NginxWebServer {
                address 192.168.12.9/32;
            }
            rule-set DestinationNATRuleWebServer {
                from zone untrust;
                rule ruleToWebServer {
                    match {
                        destination-address 10.56.16.80/32;
                        destination-port {
                            80;
                        }
                    }
                    then {
                        destination-nat {
                            pool {
                                NginxWebServer;
                            }
                        }
                    }
                }
            }
        }
    }
    policies {
        from-zone untrust to-zone DMZ {
            policy WebServerPolicy {
                match {
                    source-address any;
                    destination-address WebServer;
                    application junos-http;
                }
                then {
                    permit;
                }
            }
        }
        from-zone DMZ to-zone untrust {
            policy outOnTheInternet {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone DMZ to-zone trust {
            policy default-permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone trust to-zone DMZ {
            policy default-permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone trust to-zone trust {
            policy default-permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone untrust to-zone trust {
            policy default-deny {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    deny;
                }
            }
        }
        from-zone trust to-zone untrust {
            policy internet-access {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
    }
    zones {
        security-zone DMZ {
            address-book {
                address WebServer 192.168.12.9/32;
            }
            interfaces {
                ge-0/0/3.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                            ssh;
                            dhcp;
                        }
                    }
                }
            }
        }
        security-zone trust {
            host-inbound-traffic {
                system-services {
                    ping;
                    ssh;
                }
            }
            interfaces {
                ge-0/0/2.0 {
                    host-inbound-traffic {
                        protocols {
                            ospf;
                        }
                    }
                }
                ge-0/0/6.0;
                ge-0/0/4.0 {
                    host-inbound-traffic {
                        system-services {
                            dhcp;
                        }
                    }
                }
            }
        }
        security-zone untrust {
            interfaces {
                ge-0/0/5.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                            ssh;
                        }
                    }
                }
            }
        }
    }
}
access {
    /* DHCP parameters to be handed out. */
    address-assignment {
        pool DMZ {
            family inet {
                network 192.168.12.0/24;
                range USERS {
                    low 192.168.12.10;
                    high 192.168.12.20;
                }
                dhcp-attributes {
                    maximum-lease-time 60;
                    name-server {
                        8.8.8.8;
                    }
                    router {
                        192.168.12.1;
                    }
                }
            }
        }
        /* DHCP pools */
        pool USERLAN {
            family inet {
                network 192.168.13.0/24;
                range USERS {
                    low 192.168.13.10;
                    high 192.168.13.20;
                }
                dhcp-attributes {
                    maximum-lease-time 3600;
                    name-server {
                        /* 8.8.8.8; */
                        192.168.11.5;
                    }
                    router {
                        192.168.13.1;
                    }
                }
            }
        }
    }
}
