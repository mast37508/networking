# This program retrieves Junos arp info by RPC in XML format
# Per Dahlstroem V01
from jnpr.junos import Device
from lxml import etree # XML Element tree
from pprint import pprint

hostIP = '10.217.19.162'

def main():
    # Test for 3 seconds if device is TCP reachable
    Device.auto_probe = 3
    # Instantiate a Device object
    dev = Device(host = hostIP, user = 'root', password = 'Rootpass') 
    print ('Opening connection to ', hostIP)
    dev.open()
    myConfig = dev.rpc.get_config(options={'format':'text'})
    x = etree.tostring(myConfig, encoding='unicode', pretty_print=True)
    print(type(x))
    print(x)
    dev.close() # Close connection but keep the Device object. Reopen with open()

main()

