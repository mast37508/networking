## Last changed: 2019-11-06 07:02:55 UTC
## Used for the Ansible test: http://jedelman.com/home/juniper-vsrx-automation-with-ansible/
version 12.1X47-D15.4;
system {
    host-name vSRX-XX;
    root-authentication {
        encrypted-password "$1$q9fEp/ix$/sctvx3BdBIhBVp9ySlVI1";
    }
    services {
        ssh;
        netconf {
            ssh {
                connection-limit 5;
            }
        }
        dhcp {
            router {
                192.168.1.1;
            }
            pool 192.168.1.0/24 {
                address-range low 192.168.1.20 high 192.168.1.30;
                maximum-lease-time 3600;
                default-lease-time 3600;
                name-server {
                    8.8.8.8;
                }
            }
        }
    }
}
interfaces {
    ge-0/0/1 {
        unit 0 {
            family inet {
                address 192.168.1.1/24;
            }
        }
    }
    ge-0/0/2 {
        unit 0 {
            family inet {
                address 192.168.2.1/24;
            }
        }
    }
    ge-0/0/4 {
        unit 0 {
            family inet {
                address 192.168.4.1/24;
            }
        }
    }
    ge-0/0/5 {
        /* Connect to "schools" network */
        unit 0 {
            family inet {
                address 10.217.128.5/22;
            }
        }
    }
}
routing-options {
    /* Default route to school gateway */
    static {
        route 0.0.0.0/0 next-hop 10.217.128.1;
    }
}
security {
    nat {
        source {
            rule-set trust-to-untrust {
                from zone trust;
                to zone untrust;
                rule rule-anyIP-to-anyIP {
                    match {
                        source-address 0.0.0.0/0;
                        destination-address 0.0.0.0/0;
                    }
                    then {
                        source-nat {
                            interface;
                        }
                    }
                }
            }
        }
    }
    policies {
        from-zone trust to-zone trust {
            policy allow_any {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone trust to-zone untrust {
            policy allow_any {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
    }
    zones {
        security-zone trust {
            interfaces {
                ge-0/0/1.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                            ssh;
                            netconf;
                            dhcp;
                        }
                    }
                }
                ge-0/0/2.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                        }
                    }
                }
            }
        }
        security-zone untrust {
            interfaces {
                ge-0/0/5.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                            dhcp;
                            netconf;
                            ssh;
                        }
                    }
                }
            }
        }
    }
}