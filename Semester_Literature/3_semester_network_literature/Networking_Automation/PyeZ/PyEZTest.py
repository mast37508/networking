from jnpr.junos import Device
from pprint import pprint
import sys

#hostIP = '192.168.2.1' Both adresses give acces to srxA-2
hostIP = '172.18.2.2'

try:
    # default device to connect, added timeout for device
    Device.auto_probe = 3 
    dev = Device(host = hostIP, user = 'root', password = 'rootpass12') 
    print 'Opening connection to ',hostIP
    dev.open()
    pprint(dev.facts)
except Exception as somethingIsWrong:
    print "Unable to connect to host:", somethingIsWrong
    sys.exit(1)

print dev.cli("show interfaces terse", warning=False) # Disable warnings
dev.close()

