1    # taking Netmiko for a spinn on Python2.7 
2     
3    from netmiko import ConnectHandler 
4    from datetime import datetime 
5     
6    juniper_srx = { 
7         'device_type': 'juniper', 
8         'ip':   '10.10.1.1', 
9         'username': 'root', 
10        'password': 'rootpass12', 
11   #     'port': 9822,               # there is a firewall performing NAT in front of this $ 
12        'verbose': False, 
13    } 
14    
15   all_devices = [juniper_srx] 
16    
17   start_time = datetime.now() 
18   for a_device in all_devices: 
19        net_connect = ConnectHandler(**a_device) 
20        output = net_connect.send_command("show arp") 
21        print "\n\n>>>>>>>>> Device {0} <<<<<<<<<".format(a_device['device_type']) 
22        print output 
23        print ">>>>>>>>> End <<<<<<<<<" 
24    
25   end_time = datetime.now() 
26    
27   total_time = end_time - start_time