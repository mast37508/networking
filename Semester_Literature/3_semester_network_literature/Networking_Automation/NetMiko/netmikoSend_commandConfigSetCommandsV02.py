#!/usr/bin/python3
# Configuration mode is entered and show | display set
# is issued to list configuration set commands
# The set commands are then written to a file named srxConfigSetCommands.set
# By Per Dahlstroem 2019

import netmiko

myConnection = netmiko.ConnectHandler(ip='192.168.2.1',
                                      device_type='juniper',
                                      username='root',
                                      password='Rootpass')
myConnection.config_mode()
configSetCommands = myConnection.send_command('show | display set')
# with open("SRXconfigurations/srxConfigSetCommands.set", "wb") as f:
with open("/home/per/programmes/SRXconfigurations/srxConfigSetCommands.set", "wb") as f:
    f.write(configSetCommands.encode("UTF-8"))
myConnection.exit_config_mode()
myConnection.disconnect()