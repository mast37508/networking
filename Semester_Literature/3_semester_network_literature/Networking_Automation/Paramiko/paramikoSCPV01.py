import paramiko, time, os

try:
        sshClient = paramiko.SSHClient()
        sshClient.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        print 'Trying to connect to 10.10.1.1'
        sshClient.connect('10.10.1.1', username='root', password='rootpass12')

        channel = sshClient.invoke_shell()
        print 'SRX shell invoked'
        time.sleep(1)

        os.system('clear')

        routerOutput = channel.recv(1000) # Read router replies - empty buffer

        config = '/config/juniper.conf.gz'
        configCopy = '//home/per/juniper4.conf.gz'

        channel.send('scp ' + config +' per@10.10.1.9:' + configCopy +'\n')
        time.sleep(1)

        channel.send('perpass\n')
        time.sleep(1)

        output = channel.recv(1000)
        print output
        channel.close()

except Exception as  ex:
        print 'Something went wrong.'
        print ex
