# Index for Networking videoes on Youtube

Videoes are made by: Per Dahlstrøm.

They, the videoes, are audio-visual guides to assignments which will give the viewer hands on experience with practical networking tasks.

The videoes form a course for people with now experience, but an interest in, computer networking. (From Zero to Hero! :-)

Use the keyboard combination: "ctrl" + "f" and typa e search keyword to find a video on a specific topic.

Please email me pda@ucl.dl with a topic that you would like me to make a video about.

## The assignments should be worked through in the following order:  

1. Assignment 3: https://www.youtube.com/watch?v=Ynzr2ccSvww  
2. Assignment 2: https://www.youtube.com/watch?v=I_kPqqgFBCg  
3. Assignment 1: 
4. Assignment ?: VMWW Bridge.
3. Assignment 4: 

## Assignment 1

### Ass1 Part 1: Introduction to assignment 1.

Prerequisites: It is required to have done assignment 2 and 3 before assignment 1.  

The introduction is a very high level view on IP addresses, MAC addresses and the ARP table. It is thus with very few details about the use of MAC addresses.


## Assignment 2

### Ass2 Part 1: Introduction to assignment 2.  

Prerequisites: It is required to have done assignment 3 before assignment 2.

### Ass2 Part 2: Equipping the Xubuntu base Virtual Machine (VM) with networking software.  

(01:08) Check networking parameters  
(01:36) Linux update: sudo apt update  
(01:57) Linux upgrade: sudo apt upgrade  
(02:27) Install Wireshark  
(03:08) Test Wireshark installation  
(03:46) Install tcpdump  
(04:03) Test tcpdump installation  
(04:33) Install putty  
(04:48) Test putty installation  
(04:50) Install net-tools  
(05:10) Do not install brctl as it is in bridge-utils  
(05:18) Install bridge-utils  
(05:27) Install iproute2  
(05:33) Install curl  
(05:38) Install ufw  

### Ass2 Part 3: Drawing our network diagram in Visio. Network IP ID address and Host IP ID address.  
(00:18) Ass2 Network    
(00:38) Network design
(00:59) Network documentation. (At this stage in the networking course)  
(01:30) Network IDs and Host IDs.  
(01:44) Hosts IPs  
(02:35) DHCP dynamic automatic IPs versus Static IPs documentation.  
(03:25) VMnet8 Network Editor setting repetition.  

### Ass2 Part 4: Create and configure Xubuntu VMs from the Xubuntu base VM clones.  
(00:40) First clone of base Xubuntu, creating PC1  
(01:30) Second clone of base Xubuntu, creating PC2  
(02:18) PC1 network settings set to DHCP.  
(02:42) PC2 network settings set to DHCP.  
(03:30) Configure PC2 network profile settings for a static IP.  
(06:00) Configure PC1 network profile settings  for a static IP.  
(07:00) Ping communication test between PC1 and PC2  


### Ass2 Part 5: Testing network connectivity and monitoring network IP packets in Wireshark.  

Enable broadcast ping replies: sudo sysctl -w net.ipv4.icmp_echo_ignore_broadcasts=0  

Go here to see how to set time and font: https://www.youtube.com/watch?v=16NSFej9WlM  

(03:45) Setting time reference.  
(04:45) Setting Font size in Wireshark.  

### Ass2 Part 6: Trouble shooting the network.  

TBD




 

## Assignment 3

### Ass3 Part1 Introduction.

An overview of VMware workstation on you Lap Top and where virtual devices
are and how they connect to the internet. It is a very high level work through without any technical details.  

### Ass3 Part2 Installing VMware Workstation.  

A short rundown on installing VMware Workstation 16 Pro on Windows 10.  

### Ass3 Part3 Installing VM on VMware Workstation.   

### Ass3 Part4 Set the Xubuntu terminal and keyboard layout.    

Set the Xubuntu terminal layout to black writing on white background and
set the keyboard layout to reflect your physical keyboard. 

### Ass3 Part5 Connecting a Xubuntu Linux Virtual Machine to the Internet via VMnet8.  

#### Two corrections/errata/extra info:   

(04:55) Do NOT tick: "Connect a host virtual adapter ti this network"    
(05:00) Before setting the "DHCP Settings", set the Subnet IP: 10.56.16.0 and Subnet mask: 255.255.252.0.  


## Assignment 10

Ass10 Part1 Introduction  

Prerequisites:  
Assignment  


Ass10 Part2  
Very fundamentals of routing and the Junos SRX routing table
with just one router and two subnets.  

Very fundamentals of routing with one router and two subnets. A simplified IP packet is shown being routed from one subnet to another. Explaining the routing process by only looking at the IP addresses involved. A generic routing table is shown drawn on in the explanation.  

Prerequisites:  
Assignment  

Ass10 part5 Installing and interconnecting vSRX router to PCs in VMW workstation
(02:24) Physical SRX interfaces.  
(03:35) Installing vSRX on VMWare Workstation VMWW.  
(04:40) Virtual router vSRX interfaces in VMWW.  
(05:50) Connecting virtual router vSRX interfaces to VMnets.  
(07:35)Connecting virtual router ge-0/0/2 vSRX interfaces to VMnet.  

Prerequisites:  
Assignment  

Ass10 part8  
Download and upload full configuration from and to the vSRX in Putty
Save config from Putty to text editor.  


Ass10 Part9  
We are looking into the routing process with Wireshark and with one
router and two subnets. We see how the mac addresses work on layer 1,
the data link layer, and the IP addresses work on layer 3, the networking layer.
Following a ping request frame through a simple one router network.
Contemplating IP addresses and MAC addresses through the routing process.
Showing the IP addresses and MAC addresses in Wireshark through the routing process.

Ass10 Part11 Configure host-inbound-traffic system-services,
enable and disable, ping on SRX

Disable and enable ping replies from the gateway on
SRX touching on security zones trust.
Excelent way to test basic connectivity between
networking devices and the router interfaces.
security-zone trust (00:05)
interfaces host-inbound-traffic system-services (01:00)
pinging default gateway (01:26)
configure host-inbound-traffic system-services (01:38)
disable srx interface ping replies (01:38)

I am so sorry for the slightly distorted sound. :-(

Prerequisites:
Assignment


Ass10 Part12 Explaining SRX routing table and listing
host-inbound-traffic system-services

Explaining SRX routing table and also looking at how to "understand" SRX
local system ping service/program. We also look at SRX what 
host-inbound-traffic system-services are available. For configuration of
host-inbound-traffic system-services please see part 11.
cli edit command: run show route (00:16)
cli edit command: run show route terse (00:23)
direct route (00:46)
active route (00:46)
entire subnet on an interface (02:09)
Details on available addresses in a subnet. (03:00)
local via ge-0/0/x (04:10)
ping pinging a local interface (05:50)
Listing the srx host-inbound-traffic system-services (06:52)

Prerequisites:
Assignment

## Assignment 11

Ass11 part1 The overview of how two routers do
inter router routing.
Understand the fundamentals in routing between routers
to subnets. 
What I also call inter router routing.
The routing tables are explained for two 
routers and their four subnets.
This video does not contain any configuration
for the actual routers.

Prerequisites. To follow the content, a knowledge level corresponding to that
achieved through the following sources is required:
Assignment 10.

Ass 11 Part2
We look at how to construct the network with two routers 
and 4 hosts on 4 subnets. Wec also see how we interconnect 
the routers. I am using VMnets in workstation. Interconnections 
can also be done with VMWW  Lan segments.

Prerequisites. To follow the content, a knowledge level corresponding to that
achieved through the following sources is required:
Assignment 10.


Ass 11 Part3 Using Lan segments

Prerequisites. To follow the content, a knowledge level corresponding to that
achieved through the following sources is required:
Assignment 10.

Ass 11 Part4
It is 25 minutes long as I show some intermediate simple
tests along the configuring.
Configuring static routes in SRX
More on Linux routing table:
  Gateway
  Link-local IP addresses and route
SRX CLI:
  replace pattern statement.
  run show route terse
  
Prerequisites. To follow the content, a knowledge level corresponding to that
achieved through the following sources is required:
Assignment 10.
  

Ass 11 Part5
Configure Junos/SRX routers from a file.
Edit configurations in NotePad++ text editor
Upload to Junos/vSRX.
Brief ping test end to end of configurations.
Configurations can be found on GitLab.

Prerequisites. To follow the content, a knowledge level corresponding to that
achieved through the following sources is required:
Assignment 10.
  
Ass 11 Part6
Traceroute IP address.
A demonstration of the Linux traceroute command
that will show what hops an IP packet traverses from
source IP to destination IP address.
The command is demonstrated on both Linux hosts
and on the SRX router in its Linux operating system.

Prerequisites. To follow the content, a knowledge level corresponding to that
achieved through the following sources is required:
Assignment 10.

Ass 11 Part7
Wiresharking a frame through the network.
Looking up the MAC and IP addresses in a frame in Wireshark
at a links the frame is passing when routed through
a network with two routers.
A ping request is followed through the network
from source device to destination device.

Prerequisites. To follow the content, a knowledge level corresponding to that
achieved through the following sources is required:
Assignment 10.

Ass11 Part8

Testing connectivity from subnet hosts to SRX router with the monitor command.
Testing connectivity from subnet hosts to SRX router with the tcpdump command.
Both commands run on the SRX.
(01:35) Testing using arping opposed to ping.
(02:30) Running tcpdump on SRX on its Linux operating system.

I encourage you to do the testing on your own network as I demonstrate.

Prerequisites. To follow the content, a knowledge level corresponding to that
achieved through the following sources is required:
Assignment 10.

Ass 11 Part9 Static routing troubleshooting:
Ping through the network step by step.
Linux IP address.
Linux route table.
Linux ARP table.
SRX routing table.
SRX interfaces. Are they up or down.
SRX arp table.

Prerequisites. To follow the content, a knowledge level corresponding to that
achieved through the following sources is required:
Assignment 10.



## Assignment 18

Ass18 Part1 Introduction to DHCP
(00:30) What are static IPs demoed by pinging static IP addresses
(00:51) Checking static IP settings on Linux PC3
(01:30) Checking static IP settings on Raspberry Linux PC2
(02:17) Justifying DHCP on network with multiple devices.
(03:24) DHCP in action on DHCP clients
(04:00) Switching Raspberry Linux PC2 from Static IP to DHCP
(05:16) Switching Raspberry Linux PC3 from Static IP to DHCP

Prerequisites. To follow the content, a knowledge level corresponding to that
achieved through the following sources is required:
Assignment 11.

Ass18 Part2 Configure legacy DHCP on SRX from file and configure DHCP on Xubuntu Linux. 
Walkthrough of legacy configuration in text editor. (00:30)
DHCP pools walkthrough (01:20)
Default lease time (02:40)
Configure router from putty terminal load override terminal (03:44)
Check on PCs that they receive IP configs from DHCP server (04:30)
Set an interface connection profile for DHCP on PC3 (04:50)
Verify IPV4 settings in Xubuntu GUI (06:25)
Renew IPV4 DHCP on PC4 in Xubutu GUI (07:34)
Verify IPV4 settings in network manager CLI (08:50)

Quiz question not answered in the video:
How does the router know from which pool to hand out IPs to what interface?

Prerequisites. To follow the content, a knowledge level corresponding to that
achieved through the following sources is required:
Assignment 11.

Ass18 Part3 Understand security zones host-inbound-traffic services

Give access to the DHCP system service through an interface (00:00)
A list of available services on vSRX V12: (0:31)

Prerequisites:
Assignment 11.

Ass18 Part 6 Configure extended DHCP on SRX router.
The Legacy configuration (00:00)
The Extended configuration (00:20)
System services dhcp-local-server group (00:45)
Access address-assignment pool (01:48)
security zones ... system-services dhcp (02:50)
Upload configuration to router (03:05)
Check the router DHCP configuration on Linux client Xubuntu (03:40)

Prerequisites. To follow the content, a knowledge level corresponding to that
achieved through the following sources is required:
Assignment 11.


Ass18 Part7 Configure DHCP relay on SRX from file

DHCP services and ports shown in the TCP/IP stack (01:18)
R2 relay configuration walk through (02:35)
R1 DHCP service configuration walk through (03:58)
Upload of configuratiions to R1 and R2 (06:30)
Document IPs in network diagram (07:29)
Changing IP low and high for Net4 (07:50)
Testing thatv DHCP work for all four subnets (08:24)
Monitor DHCP service on SRX (09:03)


Prerequisites. To follow the content, a knowledge level corresponding to that
achieved through the following sources is required:
Assignment 11.

Ass18 Part 5 Monitor DHCP traffic on SRX routers.
Overview of where to monitor (00:04)
List or monitor R1 DHCP mac address to IP bindings (00:28)
List DHCP server service statistics (00:48)
Deciding on a traffic filter to filter for DHCP packets (01:10)
Spawn some DHCP traffic (01:57)
dhclient -v -r ens33 release IP address (02:35)
dhclient -v ens33 renew IP address (02:50)
bytes missing (03:25)
A bit of DHCP traffic origin analysis (03:30)
Monitoring traffic on the DHCP relay on R2 (03:50)
Deciding again on a traffic filter to filter for DHCP packets (04:21)
dhclient -v -r ens33 release IP address (05:00)
dhclient -v ens33 renew IP address (05:05)



Prerequisites. To follow the content, a knowledge level corresponding to that
achieved through the following sources is required:
Assignment 11.


Ass18 Part 8 Monitor DHCP DORA process in Wireshark
DHCP client and server ports. (00:34)
Starting Wireshark. (01:20)
Wireshark filtering on DHCP port numbers. (01:40)
Wireshark renew IP from Xubuntu GUI. (02:20)
Knowledge of TCP/IP stack helps create Wireshark filters. (03:30)
Monitor renewing IP by linux dhclient program. (03:57)
At (04:50) the DHCP server destination address is 192.168.12.1. Ignore then 10.10.10.11. :-)
DORA at (05:25)

Prerequisites:
Assignment 11.

## Assignment 30

Ass30 Part 1
Introduction to assignment 30, Source Nat and default route.

Please find the basic configuration on GitLab.

Ass30 Part 2 Configuring VMnet8 for NAT and configuring Xubuntu Linux
https://www.youtube.com/watch?v=3XL-pzUq8Vs&ab_channel=PerDahlstroem

Configuring VMnet8 for NAT and configuring Xubuntu Linux
with static IP and gateway and to point at a DNS server.

Adding a device to VMnet8 for testing internet access. (00:30) and (01:05)

PLEASE NOTE that "VMnet8 = R1". R1 is a VM Ware Workstation thing.
I.e. the 10.56.16.0/22 network alias VMnet8 is provided by R1.
R1 is a diagram symbol to illustrate the VMWW build in router functionality.
The "outside" interface of R1 is thus the "LabTop" interface.
R3 is a home, school or workplace router.

Explaining what VMnet8 is in the network diagram. (00:40) to (00:52)

Configure VMnet8 (02:02)
Configure NAT on VMnet8. (03:10)
Configuring Xubuntu Linux with static IP and gateway
and to point at a DNS server. (04:00)
Ultra short recap of what NAT does. (05:33)




Ass30 Part 3 Configure Zone and Policy on srx in CLI
https://www.youtube.com/watch?v=2x3R8WttSd0&ab_channel=PerDahlstroem

Please find the basic configuration on GitLab.

Recap of default default gateway functionality. (00:08 to 00:50)
Pretesting the network for subnets connectivity. (01:20)
Listing starting point configuration. (02:05)



We "prepare" our configuration for setting up default
route and for NATing from zone trust to zone untrust.
Part 4 will show configuring Default Route and NAT.

Ass30 Part 4 Configure Default Route and NAT on SRX in CLI.

Configure Default Route (00:24)


Ass30 Part 5 Configure Zone and Policy and Default Route and NAT on SRX from file





Ass30 Part 6 Explaining Junos Default Route and NAT

## Assignment 53

Ass53
Part4
Prerequisites: Ass53 Part1
Installing nginx web server on raspberry Pi
Uninstalling nginx web server on raspberry Pi
Status on nginx web server on raspberry Pi
Start nginx web server on raspberry Pi
Stop nginx web server on raspberry Pi
Connecting a VM to the Internet via VMnet8
Connecting a VM back to a LAN segments after a VMnet8 internet connection

Ass53
Part5
Prerequisites video: Ass53 Part1 and Part4
Python http.server setup
Explaining what the index.htlm file is
Creating a python program for a simple dynamic web page
Creating a script that will write a random number to index.html
Creating an index.html page.
Creating a css Cascading Style Sheet file. 
Starting and stopping the python web server.

Ass53
Part6 The role of layer 4 TCP ports in TCP/IP communication
Prerequisites video: Ass53 Part5
Here, at a high level, we look at:
Application layer 5 applications. Browser and Web server.
TCP port numbers assignment and importance or role.
Transport layer 4 multiplexing of traffic and port numbers.

Ass53
Part7 Wiresharking http Web server traffic to and from RaspberryPi Web Server.
Short (02:00 min.) recap of Web server mechanics. See full explanation in part 6.
Looking at communication between webbrowser Firefox and http.server python web server.
Capturing and dissecting tcp connection carrying http traffic.
Looking at what ports are used in Wireshark.
Three way hand shake connection set up, seen in wireshark.
Four way hand shake connection close or take down.
Filtering out http traffic in Wireshark. (05:01)
Follow http stream in Wireshark and revealing frame data content. (08:00)
Communication through the whole TCP/IP stack. (07:45)
A remark on the insecurity of the python http.server (08:50)

## Assignment 54  

Ass54
Part1
Demo in VMWare Workstation.
Very simple security specifications and testing the specifications.
It is not a full specification test being demoed.
Pinging and fetching web pages.
Observing how SRX is configured to block certain traffic.
Assignment 11.
Assignment 53.

Ass54
Part2
Implementation of very simple security specifications.
Explaining in depth one zone and its policies.
Explaining Address Book.
Explaining custom defined Applications protocol properties configuration.
Configure application properties at the [applications] hierarchy level.
Entire security configuration for simple security specifications is shown.
The policy shown from assignment 11 allows all or any traffic to transit. E.g. at (06:25)
Prerequisites::
Assignment 11.
Assignment 53.

Ass54
Part3
Configuring multiple zones on SRX.
One minute recap of one zone configuration from part 1.
Configuring policies for inter zone traffic transition.
Example of configuring policies to comply to security specifications.
Application opening for udp traffic to enable "traceroute" (03:04 to 03:16) 
Policies are controlling transit traffic from interfaces in one Zone to interfaces in another Zone!
In this video the naming of zones unfortunately might create some confusion. In most cases I refer to the zones and not the subnet when I use the subnet names. :-(
Prerequisites:
Assignment 11.
Assignment 53.

## Assignment 55

Ass55
Part1 Introduction Basic MQTT devices on VMWW bridged network
Demo of a bridged network on VMware Workstation
Demo of Python MQTT Publisher and Subscriber and the mosquitto broker or server.
In this video there are almost no technical details. It is a test use case demo.
Please note that the "MQTT" network could be used without being bridged to the host computer network.
Prerequisites:
An basic understanding of VMware Workstation.
An basic understanding of Linux.

Ass55
Part2
Setting up VMnet0 on VMware Workstation for bridging.
Setting 3 devices, here named PCs, for static IPs and one device for DHCP.
Setting 192.168.1.1 as the default gateway on devices. (06:00)
Setting DHCP (09:51)


Ass55
Part3
Demo of MQTT goal system. Please see video Ass55 Part 1.
Demo of MQTT goal system with SSH to clients (00:02)
Mosquitto broker installation (03:24)
Mosquitto status command
Custom subscriber Python Program source code creation (05:29)
Custom publisher Python Program source code creation (07:33)
Not included:
MQTT protocol explanations.
Python source code explanations.
Prerequisites:
Ass55 Part1.
Ass55 Part2.

## Assignment 56

Ass56
Part1
Demo of MQTT goal system devices transactions. 
Smart phone apps demo
EasyMQTT
Mqter
Capturing and displaying MQTT traffic with Wireshark. (05:40)
Wireshark MQTT && IP filter (06:08)
MQTT Version 5.0 OASIS Standard. (06:47)

Ass56
Part2
Install and test app on Smart Phone.
Install EasyMQTT (00:40)
EasyMQTT publishing. (01:40)
Python Subscriber source code (02:30)
EasyMQTT subscribing. (03:00)
Python publisher source code. (04:00)

Prerequisites:

Ass56
https://www.youtube.com/watch?v=16NSFej9WlM
Assignment 56 Part 3 Hands on MQTT packet capturing in Wireshark
Part3
Publisher program. (00:49)
Set a capture filter. (02:00)
Setting time reference. (03:45)
Setting Font size in Wireshark. (04:45)
Contemplating network diagram from a TCP/IP perspective. (05:10)
Looking for publisher TCP port in Wireshark (06:40)
A walk through of the Wireshark layout. (08:10)
Dissecting a MQTT Publish Message packet in some details. (09:00)
MQTT Publish Message Flags. (09:31)
Flags settings compared to Python program settings (10:00)
Looking at the Publish Message format in standard paper. (11:00)

Ass56
Part4
Hands on capturing of healthy MQTT traffic with Wireshark.
A walkthrough of the Python MQTT publisher program to identify what
parts of the program are responsible for what commands or packets in
the packet capture.


Prerequisites:
Assignment 55

## Assignment 58

Ass58
Part1
This is only a demonstration of using SSH host keys for login to a ssh server,
which we are going to configure here in assignment 58.

Prerequisites:
Assignment 10

## Assignment 59

Ass59
Part1
This is only a demonstration of using SSH client keys for password free login to a ssh server,
which we are going to configure here in assignment 59.


Prerequisites:
Assignment 58

Ass59
Part2
Walkthrough of how to set up client keys for password free login to known ssh servers.

Prerequisites:
Assignment 58



