---
title: 'Network SRX security'
subtitle: 'Linux and Windows'
authors: ['Per Dahlstrøm \<pda@ucl.dk>']
main_author: 'Per Dahlstrøm'
date: \today
email: 'pda@ucl.dk'
left-header: \today
right-header: 'Network Junos security'
---

By Per Dahlstroem pda@ucl.dk

# Audience

The main audience is students at the two year IT Technology education at UCL in Odense Denmark ucl.dk/international

---

# Purpose

The purpose of this document is to provide students with an entry level introduction to network security on a Junos device.

---

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>

# Example network

This generic network diagram serves as the foundation for describing and demonstrating Junos security on Junos security devices.

The network consists of three routes R1, R2 and R3. R1 and R2 are interconnected through network 10.10.10.0/24. R2 and R3 are interconnected through network 10.56.16.0/22. PC3 and PC 4 are both debian Xubuntu Linux machines. The following instructions are for these two machines if nothing else is stated.

The network was build in VMware Workstation.

![Semantic description of image](Junos_images/Junos_generic_security_zones.JPG "Generic network diagram")  

---  

# Security zones

On the SRX router R2 in the diagram above three security zones are illustrated:

* trust
* untrust
* DMZ

The zone names are decided by the user but the default SRX configuration has trust and untrust configured. The names "trust" and "untrust" can freely be changed.

A zone is an abstract concept or in IT terms a logical entity. A zone holds one or more interfaces. In the example below an example configuration for the trust and the untrust zones are shown. In this example there is only one interface in each of these zones, but multiple interfaces can be in one zone.

![Semantic description of image](Junos_images/Junos_trust_untrust_zone.JPG "")

The reason for putting different interfaces into different zones is that different security settings can be applied to these interfaces or groups of interfaces. In other words, it can be decided at interface or zone level what TCP/IP traffic to allow through the interfaces belonging to a zone.

Here is shown a way to configure multiple interfaces in a zone:

![Semantic description of image](Junos_images/Junos_host_inbound_traffic_ge-0-0-2_and_4.JPG "")

Due to dhcp here only applying for ge-0/0/4, the configuration here allow dhcp traffic in on ge-0/0/4 but not on ge-0/0/2. Both interfaces are configured to allow ping requests but only ge-0/0/2 receives ssh connections in. For ge-0/0/4 only dhcp and ping is alolwed in. See here below.

## Controlling Inbound Traffic Based on Traffic Types  

Host-inbound-traffic system-services is checked only when the inbound traffic destination is the system service on the SRX itself. If the traffic is for the same service but destined for a device on a subnet this traffic will not be checked by the zone settings. In the illustration below acces to the ping service on the SRX is blocked, but the green pings from PC4 to PC3 passes unaffected by the trust zone settings shown. Note that ping is outcommented at "global" level and removed from ge-0/0/4 and thus blocked. It is the latter setting that is blocking pings in to system-service on ge-0/0/4. The "global" setting only affects ge-0/0/2.(?)

![Semantic description of image](Junos_images/Junos_host_inbound_traffic.JPG)

 It works for the PC3-ge-0/0/3 configuration.  

![Semantic description of image](Junos_images/Junos_host_inbound_traffic_ge-0-0-3.JPG)

Security devices like the Junos security devices execute the various parts of the security configurations in a configuration in a given sequence or order. The below diagram shows that the configuration in the zones is executed after the destination nat and the routing.

![Semantic description of image](Junos_images/Junos_SRX_packet_processing.JPG)

Source:  
https://www.juniper.net/documentation/en_US/junos/topics/topic-map/security-zone-configuration.html

---
# Security policies

 Security policies enforce rules for transit traffic, in terms of what traffic can pass through the firewall from one zone to another zone. Security policies enforce actions that need to take place on traffic as it passes through the firewall from zone to zone. 

 By default, interfaces in a zone are blocked from sending traffic to another zone. One or more policies must be set for a zone to unblock the traffic.  

Each policy is associated with the following characteristics:

* A source zone  
* A destination zone  
* One or many source address names or address set names  
* One or many destination address names or address set names  
* One or many application names or application set names  

In the following policy all traffic is permittet:

![Semantic description of image](Junos_images/Junos_policy_trust_DMZ_permit.JPG "") 

This means that e.g. PC4 can fetch a web page on PC3 and PC4 can ping PC3.  

To let only http traffic through from trust zone to DMZ zone, set the application junos-http match condition. This will block all other traffic than http:

![Semantic description of image](Junos_images/Junos_policy_trust_DMZ_http_permit.JPG "") 

DO NOT set a firewall rule later to do packet mode or based inspection for HTTP and ICMP traffic, obviously. They will be blocked as HTTP and ICMP are session based.


Source:  
https://www.juniper.net/documentation/en_US/junos/topics/topic-map/security-policy-configuration.html  
https://www.juniper.net/documentation/en_US/junos/information-products/pathway-pages/security/security-policies-feature-guide.html

# Flow or packet based mode

The Junos SRX can operate on incomming and outgoing traffic in two modes:

* Packet base. A per packet independent operation.
* Flow based. A packet flow operation on a sequence or session of packets where operations are interdependent on previous operations on previous packets.

To see what mode the SRX is operating in, run the command:

root@vSRX-R2> `show security flow status`

![Semantic description of image](Junos_images/Junos_flow_or_packet.JPG "")  

The SRX can be configured for both flow based and packet base simultaiously.

Flow mode operation is required when the SRX is used with the following features used and configured:

* Security Zones
* Security policies
* Statefull Firewalling
* NAT
* IPsec

In flow mode operation the SRX estabilses a session for a packet comming in on an interface and all the subsequent packets for that session or flow. E.g. if a packet is NAtted this flow or seesion is cahshed to alow the return or reply packet through the NAT process.

The SRX thus has a Session table where it stores information on all ongoing sessions or flows.

Taking a look at the session table can be used as a debugging tool. If e session is expected and no session or that session does not show up, this could be due to some misconfiguration.

Here a NAT session is expected on router R2 from zone DMZ to zone untrust:

root@vSRX-R2> `show security flow session`
Total sessions: 0

No session is showing up.  

Now running a ping to 8.8.8.8 on PC3 192.168.12.2 in the DMZ zone will yield a session in the session table:  

![Semantic description of image](Junos_images/Junos_sessions_table.JPG) 

It is left to the reader to analyse the above output. :-)

# Packet based configuration

If stateless packet inspection and handling is required, which is e.g. needed for stateless firewal operation., packet based mode can be achieved by configuring Selective Stateless Services for the required interfaces, even flow base oprration is configured for iother interfaces.

## Selective Stateless Services

All packets comming in on an interface have to pass e.g. the Class of Service filter or Stateless Firewall filters, i.e. the packet based or mode filters. This is here below ilustrated byt the red section.

![Semantic description of image](Junos_images/Junos_SRX_packet_processing.JPG)

The SRX can be configured to bypass all the flowbased lookups and just do the routing part. In the ilustration here below, if an incomming packet matches e.g. a firewall filter, it will continue to the routing part and be routes to the appropriate egress interface. Bypassing the flowe or session part. (If NAT is required for the packet this will thus not work???)

![Semantic description of image](Junos_images/Junos_selective_stateless_services.JPG)

To switch traffic for an interface to stateless treatment, set firewall filters to packet-mode. In this case the traffic from 192.168.12.2 and from 192.168.13.3 is being configured to be treated packet based in two different filters. I.e. in both directions. Further the filters are configured to count the number of packets being treated by the filters and packets are rejected. I.e. packets or traffic is being blocked. The counters are given names PC3 and PC4.

![Semantic description of image](Junos_images/Junos_firewall_stateless_filter.JPG)

These firewall filtes only take effect when the associated interfaces are being assigned the filters:

[edit interfaces ge-0/0/3 unit 0 family inet]  
root@vSRX-R2# `set filter input In_from_192_168_12_2`

ge-0/0/3 and ge-0/0/4 configurations now hold these stateless filters:

![Semantic description of image](Junos_images/Junos_firewall_ge-0-0-3_stateless_filter.JPG)

![Semantic description of image](Junos_images/Junos_generic_security_zones.JPG "Generic network diagram")  

As the filter has been set to reject, the filter does not allow any traffic to be routed from 192.168.12.2 to 192.168.13.3. A ping to 192.168.13.3 is now being filtered. Below it is shown when the filter kicks in. This is achieved by running the ping when commiting the configuration on R2.

![Semantic description of image](Junos_images/Junos_firewall_ping_filtered.JPG)

List what filters are applied to an interface:

![Semantic description of image](Junos_images/Junos_firewall_ge-0-0-3_filter_applied.JPG)

Running the ping on PC3 to PC4 will not show any session as there is no session set up as all session operation is being bypassed by the firewall rules.  

root@vSRX-R2# `run show security flow session`  
Total sessions: 0  

If e.g the firewall filter on ge-0/0/3 is disabled by outcommented it like this:

![Semantic description of image](Junos_images/Junos_firewall_ge-0-0-3_filter_disabled.JPG)

and running a few pings from 192.168.12.2 to 192.168.13.3 and listing the sessions in the sessioin table will show that the traffic from 192.168.12.2 to 192.168.13.3 is being treated as a session:

![Semantic description of image](Junos_images/Junos_sessions_table_one_direction.JPG)

To check if firewall filters are doing their job counters can be listed. Here the counter for the filter In_from_192_168_12_2 named PC3 at 192.168.12.2 on interface ge-0/0/3 is listed:

![Semantic description of image](Junos_images/Junos_firewall_filter_counter.JPG)

To check if policies are doing their job the counters can be listed:

![Semantic description of image](Junos_images/Junos_policy_hit_counter.JPG)

It is notable that far fewer packets have been handled by the policies as they are flow based and they are being bypassed by the packed based firewall rules.

Configuration:  
The full configuration can be seen in:  
20S_SRX_policies_and_firewall_V02.json  

Source:
* SRX_forwarding_Modes.mp4 = https://www.youtube.com/watch?v=YYHxcT8ZYiE
* https://www.oreilly.com/library/view/juniper-srx-series/9781449339029/ch13.html
* https://www.youtube.com/watch?v=AeZnchuloHI

---  

# Screens

TBD



