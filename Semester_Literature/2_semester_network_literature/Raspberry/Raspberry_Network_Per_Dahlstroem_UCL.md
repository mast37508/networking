---
title: 'Raspberry Networking'
subtitle: 'Linux'
authors: ['Per Dahlstrøm \<pda@ucl.dk>']
main_author: 'Per Dahlstrøm'
date: \today
email: 'pda@ucl.dk'
left-header: \today
right-header: 'Raspberry Networking'
---

# Audience

Students at the two year IT Technology education at UCL in Odense Denmark.

---

# Purpose

The purpose of this document is to provide students with a compilation of entry level tools to work with the ethernet (and later WIFI) network settings on Rapsberry Pi.

---

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>

# Example network

This generic network diagram serves as the foundation for describing network communication in this document.

![Semantic description of image](Rpi_network_images/Rpi_Generic_network.JPG "")  

---

# Some basics

From now on PC 3 is a Raspberry Pi. :-)  

PC3 can get its IP in two ways:  

* Dynamically through DHCP  
* Static set by a user on PC 3  

---

# Versions used

* VMware Workstation

??

* Raspbian version

The descriptions in this document are run on a raspbian version:

pi@raspberry:~ $ `cat /etc/os-release`  
PRETTY_NAME="Debian GNU/Linux 9 (stretch)"  
NAME="Debian GNU/Linux"  
VERSION_ID="9"  
VERSION="9 (stretch)"  
VERSION_CODENAME=stretch  
ID=debian  

---

# Configuring network setting on Raspberry

Raspberry is preconfigured to read the /etc/dhcpcd.conf network settings configuration file and configure network adapters accordingly.

It is the dhcpcd deamon or program who does this.

The name dhcpcd could mislead to beleieve that it is only for dhcp, but the dhcpcd configuration is both used to configure dhcp and also to set static IPs for interfaces.

Using the dhcpcd .conf is fine and works.

But a more powerfull and maybe convenient way to set network parameters is using the network manager either in its CLI or graphically.

---

# Kill the dhcpcd deamon

In order to not have ambiguty settings of networking interfaces, the dhcpcd program should be killed if the network Manager is used. First find the dhcpcd deamon Linux process ID:

pi@raspberry:/etc $ `ps -A | grep dhcp`  
   601 ?        00:00:00 dhcpcd  
pi@raspberry:/etc $  

Then kill the dhcpcd deamon process:

pi@raspberry:/etc $ `sudo kill 601`

---  

# Check file interfaces and kill dhcli dhcp client

Network Manager will only handle interfaces not declared in /etc/network/interfaces file.

Check the settings in /etc/network/interfaces

In this case there are no settings in the file and Network Manager will thus be able to do all the setting for interfaces on this device:

![Semantic description of image](Rpi_network_images/Rpi_etc_network_interfaces_file.JPG "")  

Also check if dhclient is running as this deamon uses the old /etc/network/interfaces settings:

pi@raspberry:/etc $ `ps -A | grep dhcli`

Kill it if it is running. Not sure if dhclient affects Network Manager as there are no settings in /etc/network/interfaces, but if dhclient is dead it will not make trouble.

---

# Network manager

The Raspberry dosnt come bundeled with Network manager.

![Semantic description of image](Rpi_network_images/Rpi_no_networkmanager_shown.JPG "")  

Install the Network Manager:

`sudo apt-get install network-manager`  

`sudo apt install network-manager network-manager-gnome`  

The Network manager will now show up in the menu as Network Connections:

![Semantic description of image](Rpi_network_images/Rpi_with_networkmanager_shown.JPG "Generic network diagram")  

At the illustrated install dhcpcd was NOT killed and beforehand and thus populated the Network Connectoions with settings:

![Semantic description of image](Rpi_network_images/Rpi_NM_GUI_network_connections.JPG "")  

Kill dhcpcd as described above.

Bring eth0 down and then up again and the (strange?) eth0 setting is gone.

![Semantic description of image](Rpi_network_images/Rpi_dhcpcd_killed.JPG "") 

Edit the Wired connection 1 and give the Wired connection 1 a better and descriptive name as shown here:

![Semantic description of image](Rpi_network_images/Rpi_edit_wired_connection_1.JPG "") 

What is actually being edited here above is a connection profile for eth0.

Alle intefaces like eth0 can have many different profiles that can be switched among. This is illustrated shortly. Only one profile can be chosen and thus active at a time. 

Reboot Linux to make the changes take effect and note how the Network manager icon now apears in the upper status bar as a socke-wire icon.  

Alternatively only do a restart of the network-manager to make the changes take effect:

$ `/etc/init.d/network-manager restart`

After a reboot check if dhcpcd is again running and active.

![Semantic description of image](Rpi_network_images/Rpi_is_dhcpcd_running.JPG "") 

And it is running and active. Kill it permanently if it is not going to be used.

## Kill dhcpcd permanently.

![Semantic description of image](Rpi_network_images/Rpi_dhcpcd_killed_permanently.JPG "") 

Reboot to make the changes take effect.

After a reboot check again if dhcpcd is running. And here it did not start again.

![Semantic description of image](Rpi_network_images/Rpi_dhcpcd_killed_reboot.JPG "")

Clicking the socket-wire icon in the top the Network Manager shows what connections are active.

Right click and edit to make a new extra connection profile for eth0. This time creating a DHCP connection profile for eth0:

![Semantic description of image](Rpi_network_images/Rpi_edit_wired_DHCP.JPG "")

The desired connection profile for eth0 can now be chosen from the top socket-wire icon menu. Here was switched to the eth0_PDA_DHCP dhcp profile for eth0:

![Semantic description of image](Rpi_network_images/Rpi_NM_switch_connection.JPG "")

---

# Two or more interfaces

If a second interface card is attach to the Raspberry in VMWW it will show up as eth1 and so forthe for a third.

The existing conection profiles will be available for this new interfaces card eth1. Choose a connection that suits or create a new one.

As in this case here both interfaces eth0 and eth1 are running on the same type of hardware here it is not easy see which one is eth0 amd which is eth1. :-( And the eth0 and eth1 names do not help much now. They are actually now missleading.)

![Semantic description of image](Rpi_network_images/Rpi_two_interfaces.JPG "")

## What interface is configured with what connection?

To explicitly see what has been configured for each interface, right click the top Network Manager symbol and select Connection Information. The naming for eth0 and eth1 can be wrong as it is set by the user.  
Luckily in the Interface line Linux finally shows the system name e.g. (eth1) for this interface. See below.  

![Semantic description of image](Rpi_network_images/Rpi_connection_Information.JPG "")

Here the Connection Information for eth1 is chosen but note that eth0 is also displayed in the lefthand neighbour tab.

![Semantic description of image](Rpi_network_images/Rpi_connection_Information_eth1.JPG "")

It is NOT shown here if the settings are dynamic i.e. dhcp or static. See above to reach this information.  

---

# Network Manager profiles

NetworkManager keeps connection information on known individual networks in configuration files called profiles. Those are stored at /etc/NetworkManager/system-connections/.  

For options or settings in these files refer to the manpage on nm-settings:

* man nm-settings or online.  

Connections or profiles can be edited using a text editor in the profiles or the nm-connection-editor or the nmcli.

Sources:  
https://wiki.debian.org/NetworkManager


# Get WIFI active on Physical Raspberry

When dhcpcd is disablede the Raspberry(?) OS will block the WIFI wlan interface. I suppose it is because dhcpcd used to configure the wlan radio WIFI interface, and as dhcpcd is removed it no longer does this.  
To get Network Manager to control and configure the wlan WIFI radio interface, this has to be unblocked.  
The command `rfkil`, meaning radio frequency kill, will do this:

`$ rfkill unblock wlan`

Here a sequence of rfkill commands to unblock the wlan rf interface and to block bluetooth are shown:

![Semantic description of image](Rpi_network_images/rfkill_unblock_wlan.jpg "")

`rfkill`  wil list the state of the networking devices alisa interfaces on the Linux box.

An interface can be:  
* software blocked or unblocked. The interface can be blocked or unblocked by the `rfkill` command.
* hardware blocked. A switch button on the devuice is blocking the or unblocking the interface.  

## Nagging message  

The OS will tell that `No wireless LAN interface found` even this is not true. I suppose it is because dhcpcd is consulted by OS and not found. 

![Semantic description of image](Rpi_network_images/wlan_not_found.JPG "")  

The wlan will be working despite the message.  

## Listing wlans

After unblocking the wlan interface the WIFI will discover the reachable WIFI networks.  

![Semantic description of image](Rpi_network_images/Discovering_WIFI_APs.JPG "")

Here an iPhone network was chosen.

## Configure the wlan

From the Edit Connections... menu the Network Connection window can be reached:

![Semantic description of image](Rpi_network_images/Rpi_connection_Information.JPG "")

Now the interface profiles can be configured in the Network Connection window.

Here a profile was named iPhone as the profile i.e. Linux configuration for the iPhone hot spot network.

![Semantic description of image](Rpi_network_images/Network_Connections.JPG "")


## Configure interfaces in nmcli in command prompt

The network manager can be operated from the commandprompt.

Please go to this document and see the nmcli section.

[https://gitlab.com/PerPer/networking/-/blob/master/Troubleshooting/Network_Troubleshooting_Per_Dahlstroem_UCL.md](https://gitlab.com/PerPer/networking/-/blob/master/Troubleshooting/Network_Troubleshooting_Per_Dahlstroem_UCL.md)


# Netplan

TBD

If you like YAML. :-)

